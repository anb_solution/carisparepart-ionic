export interface Environment {
    mode: string;
    httpEndPoint: string;
    httpStorage: string;
}