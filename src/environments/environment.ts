import { Environment } from './environment.model';

export const ENV: Environment = {
  mode: 'Production',
  httpEndPoint: 'https://gps-api.cilacap.co/gpsapi/',
  httpStorage: 'https://storage.cilacap.co/',
}