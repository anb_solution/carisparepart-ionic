import { Environment } from './environment.model';

export const ENV: Environment = {
  mode: 'Development',
  httpEndPoint: 'https://cilacap.co/gpsapi/',
  httpStorage: 'https://storage.cilacap.co/',
}