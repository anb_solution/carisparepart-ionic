import { NgModule } from '@angular/core';
import { CartMenuComponent } from './cart-menu/cart-menu';
import { IonicModule } from 'ionic-angular';
import { Colorbox } from './colorbox/colorbox';

@NgModule({
	declarations: [CartMenuComponent, Colorbox],
	imports: [
		IonicModule.forRoot(CartMenuComponent)
	],
	exports: [CartMenuComponent, Colorbox]
})
export class ComponentsModule {}
