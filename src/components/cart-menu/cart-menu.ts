import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CartPage } from '../../pages/cart/cart';
import { CartProvider } from '../../providers/cart/cart';

/**
 * Generated class for the CartMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'cart-menu',
  templateUrl: 'cart-menu.html'
})
export class CartMenuComponent {

  text: string;

  constructor(public navCtrl: NavController, public cartProvider: CartProvider) {
    console.log('Hello CartMenuComponent Component');
    this.text = 'Item';
  }

  cartTapped() {
    this.navCtrl.push(CartPage);
  }  

}
