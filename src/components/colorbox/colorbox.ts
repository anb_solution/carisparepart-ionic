import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import { CatalogProvider } from '../../providers/catalog/catalog';
import L from 'leaflet';
import * as jquery from 'jquery';

@Component({
    selector: 'app-colorbox',
    templateUrl: 'colorbox.html',
})
export class Colorbox { 
    @ViewChild('map') mapContainer: ElementRef;
    @Input() img = '';
    @Input() catalog: any;    
    map: any;
    catalogProducts: any;
    mapping: any = [];

    constructor(public catalogProvider: CatalogProvider) {
       
    }

    ngAfterViewInit() {
        this.getMapping();            
    }

    
    getMapping() {    
        console.log('star maping');
        let catalog = JSON.parse(this.catalog);
        console.log('cc', catalog);            
        const catalog_id = catalog['id'];
        const is_grouping_mode = catalog['is_grouping_mode'];
        if (is_grouping_mode == '0') {        
            this.catalogProvider.getMapping(catalog_id)
                .subscribe(data => {
                    this.mapping = data['data'];
                    this.loadmap();    
                }
            );    
        } else if (is_grouping_mode == '1') {
            this.catalogProvider.getProducts(catalog_id)
            .subscribe(data => {
                for (let cat of data['data']) {
                    for (let catalogMapping of cat['CatalogProductGroupMappings']) {
                        this.mapping.push(catalogMapping);
                    }
                }
                this.loadmap();    
            });  
        }  
    }    

    loadmap() {

        var osmUrl= this.img,
          h = 850 * 8,
          w = 600 * 8;
    
          var map = L.map('map', {
            minZoom: -1,
            maxZoom: 4,
            center: [0, -600],
            zoom: 1,
            crs: L.CRS.Simple,
            zoomControl: false,
            attributionControl: false,
          });
    
        var southWest =  map.unproject([0, h], map.getMaxZoom());
        var northEast = map.unproject([w, 0], map.getMaxZoom());
        var bounds = new L.LatLngBounds(southWest, northEast);
          
        // create the control
        var command = L.control({position: 'topright'});
        
        command.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'command');
            div.innerHTML = '<form> <input id="command" type="checkbox" checked=""/> <label for="command">Show Marker </label></form>'; 
            return div;
        };
        if (this.mapping.length > 1) {
            command.addTo(map);
        }
                      
        L.imageOverlay(osmUrl, bounds).addTo(map);
        map.setMaxBounds(bounds);
        for (let m of this.mapping) {
        
            L.marker(map.layerPointToLatLng(map.containerPointToLayerPoint([m.left, m.top])), {icon: L.divIcon({className: 'my-div-icon', html: m.archor})}).on('click', function(e){ 
                var elmnt = document.getElementById("item"+m.archor);
                elmnt.scrollIntoView();
                }).addTo(map);
        }

        if (this.mapping.length > 1) {
            document.getElementById("command").addEventListener("click", function(){ 
                if (jquery('.leaflet-marker-pane').is(":visible")) {
                    jquery('.leaflet-marker-pane').hide();
                } else {
                    jquery('.leaflet-marker-pane').show();
                }
            }, false);
        }
        
        //L.marker(map.layerPointToLatLng(map.containerPointToLayerPoint([148, 564]))).on('click', function(e){  alert('alalx') }).addTo(map);
        L.control.zoom({position: 'bottomright'}).addTo(map);
        map.setView([1, 1], 0);
          
      }    
}