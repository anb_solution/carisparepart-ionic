export interface Product {
    id: number;
    no_part: string;
    description: string;
    images: Array<string>;
}