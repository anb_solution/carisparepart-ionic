export class AddcartModel {
    constructor(
        public device_id: any,
        public product_id: number,
        public quantity: number,
      ) {  }    
}

