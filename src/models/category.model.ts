export class CategoryModel {
    id: number;
    name: string;
    image: string;
    year: string;
    enggine_type: string;
}