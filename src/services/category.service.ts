import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { ENV } from '@app/env';
import { CategoryModel } from '../models/category.model';


@Injectable()
export class CategoryService {

    constructor(
        private http: HttpClient
    ) { }    

    private categoryUrl = ENV.httpEndPoint + 'category/index';  // URL to web api
    private categoryParentUrl = ENV.httpEndPoint + 'category/parent';  // URL to web api
    private catalogTypeUrl = ENV.httpEndPoint + 'catalog/model';
    
    private log(message: string) {
        console.log('CategoryService: ' + message);
    }    
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead

        // TODO: better job of transforming error for user consumption
        this.log(`${operation} failed: ${error.message}`);

        // Let the app keep running by returning an empty result.
        return of(result as T);
        };
    }    

    /** GET categories from the server */
    getCategories (): Observable<CategoryModel[]> {
        return this.http.get<CategoryModel[]>(this.categoryUrl)
        .pipe(
            tap(categories => this.log(`fetched categories`)),
            catchError(this.handleError('getCategories', []))
        );
    }

    /** GET categories by parent. Will 404 if id not found */
    getCategoriesByParent(id: number): Observable<CategoryModel[]> {
        const url = `${this.categoryParentUrl}/${id}`;
        return this.http.get<CategoryModel[]>(url)
        .pipe(
            tap(categories => this.log(`fetched categories`)),
            catchError(this.handleError('getCategoriesByParent', []))
        );        
    }

    /** GET catalogtypes. Will 404 if id not found */
    getCatalogtypes(catagory_id: number): Observable<string[]> {
        const url = `${this.catalogTypeUrl}/${catagory_id}`;
        return this.http.get<any[]>(url)
        .pipe(
            tap(categories => this.log(`fetched catalogtypes`)),
            catchError(this.handleError('getCatalogtypes', []))
        );        
    }    
}