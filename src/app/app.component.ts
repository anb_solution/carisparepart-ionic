import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ConfirmationPaymentPage } from '../pages/confirmation-payment/confirmation-payment';
import { LoginPage } from '../pages/login/login';
import { OrderHistoryPage } from '../pages/order-history/order-history';
import { CheckOrderPage } from '../pages/check-order/check-order';
import { RegisterPage } from '../pages/register/register';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { CartProvider } from '../providers/cart/cart';

@Component({ 
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  pages: Array<{title: string, component: any, icon: string}>;
  authPages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public authServiceProvider : AuthServiceProvider, public cartProvider: CartProvider) {
    this.initializeApp();
    this.pages = [ 
      { title: 'Home', component: HomePage, icon: 'home' },
      { title: 'Payment Confirmation', component: ConfirmationPaymentPage, icon: 'logo-usd' },
      { title: 'Check Order', component: CheckOrderPage, icon: 'eye' },
    ];  
    this.authPages = [ 
      { title: 'Order History', component: OrderHistoryPage, icon: 'bookmark' },
    ]; 
  }
   
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  openLoginPage() {
    this.nav.setRoot(LoginPage);
  }

  openRegisterPage() {
    this.nav.setRoot(RegisterPage);
  }

  logout() {
    this.authServiceProvider.logout();
    this.cartProvider.setTotalItem();
    this.nav.setRoot(HomePage);
  }

}
