import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentsModule } from '../components/components.module';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { ListbrandsPage } from '../pages/listbrands/listbrands';
import { ListmodelsPage, Filter } from '../pages/listmodels/listmodels';
import { CatalogtypePage } from '../pages/catalogtype/catalogtype';
import { CataloglistPage } from '../pages/cataloglist/cataloglist';
import { CatalogDetailPage } from '../pages/catalog-detail/catalog-detail';
import { TabsPage } from '../pages/tabs/tabs';
import { ProductPage } from '../pages/product/product';
import { CartPage, UpdateCartPage} from '../pages/cart/cart';
import { CheckoutShippingPage } from '../pages/checkout-shipping/checkout-shipping';
import { RegisterPage } from '../pages/register/register';
import { CheckoutListPage } from '../pages/checkout-list/checkout-list';
import { OrderHistoryPage } from '../pages/order-history/order-history';
import { OrderDetailPage } from '../pages/order-detail/order-detail';
import { ConfirmationPaymentPage } from '../pages/confirmation-payment/confirmation-payment';
import { SearchPartPage } from '../pages/search-part/search-part';
import { CheckOrderPage } from '../pages/check-order/check-order';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { CategoryService } from '../services/category.service';
import { CatalogProvider } from '../providers/catalog/catalog';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { ProductProvider } from '../providers/product/product';
import { CartProvider } from '../providers/cart/cart';
import { UtilsProvider } from '../providers/utils/utils';
import { Device } from '@ionic-native/device';
import { OrderHistoryProvider } from '../providers/order-history/order-history';
import { ZoomAreaModule } from 'ionic2-zoom-area';
import { UploadProvider } from '../providers/upload/upload';

registerLocaleData(localeId, 'id');
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    TabsPage,
    ListbrandsPage,
    ListmodelsPage,
    CatalogtypePage,
    CataloglistPage,
    CatalogDetailPage,
    ProductPage,
    CartPage,
    UpdateCartPage,
    CheckoutShippingPage,
    RegisterPage,
    CheckoutListPage,
    OrderHistoryPage,
    OrderDetailPage,
    ConfirmationPaymentPage,
    Filter,
    SearchPartPage,
    CheckOrderPage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    BrowserAnimationsModule,
    ComponentsModule,
    ZoomAreaModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    TabsPage,
    ListbrandsPage,
    ListmodelsPage,
    CatalogtypePage,
    CataloglistPage,
    CatalogDetailPage,
    ProductPage,
    CartPage,
    UpdateCartPage,
    CheckoutShippingPage,
    RegisterPage,
    CheckoutListPage,
    OrderHistoryPage,
    OrderDetailPage,
    ConfirmationPaymentPage,
    Filter,
    SearchPartPage,
    CheckOrderPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,    
    CategoryService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: LOCALE_ID, useValue: 'id' },
    CatalogProvider,
    AuthServiceProvider,
    ProductProvider,
    CartProvider,
    UtilsProvider,
    Device,
    OrderHistoryProvider,
    UploadProvider
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule {}
