import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CatalogProvider } from '../../providers/catalog/catalog';
import { ENV } from '@app/env';
import { CatalogDetailPage } from '../catalog-detail/catalog-detail';

/**
 * Generated class for the CataloglistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cataloglist',
  templateUrl: 'cataloglist.html',
})
export class CataloglistPage {

  results: string[];
  selectedCategory: string[];
  httpStorage: string;  
  items: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public catalogProvider: CatalogProvider) {
    this.selectedCategory = navParams.get('category'); 
    this.httpStorage = ENV.httpStorage;  
  }

  ionViewDidLoad() {
    this.getCatalogs();
    console.log('ionViewDidLoad CataloglistPage');
  }

  itemTapped(event, item) {
    this.navCtrl.push(CatalogDetailPage, {
      catalog: item
    });
    console.log(item);    
  }

  getCatalogs() {
    const category_id = this.selectedCategory['catagory_id'];
    const type_id = this.selectedCategory['type_id'];
    this.catalogProvider.getCatalogs(category_id, type_id)
        .subscribe(data => {
          let datas = data['data'];
          datas.sort(function(a, b) {
            var nameA = a.name.toUpperCase(); // ignore upper and lowercase
            var nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
          
            // names must be equal
            return 0;
          }); 
          this.results = datas;
          this.items = this.results;
        }
    );      
  }

  initializeItems() {
    this.items = this.results;
  }
    
  getItems(event: any) {
    this.initializeItems();
    let val = event.target.value;
    if (val && val.trim() != '') {
      this.items = this.results.filter((item) => {
        return (item['name'].toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }  

}
