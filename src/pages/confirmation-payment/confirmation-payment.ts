import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UploadProvider } from '../../providers/upload/upload';
import { UtilsProvider } from '../../providers/utils/utils';
import { OrderHistoryProvider } from '../../providers/order-history/order-history';
import { HomePage } from '../home/home';

/**
 * Generated class for the ConfirmationPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-confirmation-payment',
  templateUrl: 'confirmation-payment.html',
})
export class ConfirmationPaymentPage {
  
  myForm: FormGroup;
  fileToUpload: File = null;
  listBank: any[];
  attachment_id: number;
  orderNumberError: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public uploadProvider: UploadProvider, private fb: FormBuilder, public utilsProvider: UtilsProvider, public loadingCtrl: LoadingController, public orderHistoryProvider: OrderHistoryProvider, public toastCtrl: ToastController) {
    this.createForm();
    if (navParams.get('order_number')) {
      this.myForm.patchValue({
        order_number: navParams.get('order_number'),
      });   
    }
  }

  createForm() {
    this.myForm = this.fb.group({
      order_number: ['', Validators.required ],
      amount: ['', [Validators.required, Validators.pattern(new RegExp('^[0-9]+$'))]],
      from_bank: ['', Validators.required ],
      from_bank_account_number: ['', Validators.required ],
      from_bank_account_name: ['', Validators.required ],
      transfer_date: ['', Validators.required ],
      transfer_to: ['', Validators.required ],
      comment: ['', Validators.required ],
      file: ['', Validators.required ],
      attachment_id: this.attachment_id
    });
  }

  get order_number() {
    return this.myForm.get('order_number');
  } 

  get amount() {
    return this.myForm.get('amount');
  }  

  get from_bank() {
    return this.myForm.get('from_bank');
  }   

  get from_bank_account_number() { 
    return this.myForm.get('from_bank_account_number');
  } 
  
  get from_bank_account_name() {
    return this.myForm.get('from_bank_account_name');
  }   
  
  get transfer_date() {
    return this.myForm.get('transfer_date');
  }  

  get transfer_to() {
    return this.myForm.get('transfer_to');
  }    

  get comment() {
    return this.myForm.get('comment');
  }    

  get file() {
    return this.myForm.get('file');
  }    


  upload(files: FileList) {
    let loading = this.loadingCtrl.create({
      content: 'Uploading...'
    });  
    loading.present();
    this.fileToUpload = files.item(0);
    let extraFields = [{'name': 'model', 'value': 'PaymentConfirmation'}];

    this.uploadProvider.postFile(this.fileToUpload, extraFields).subscribe(
      data => {
        this.attachment_id = data['data']['id'];        
        this.myForm.patchValue({
          file: this.fileToUpload,
          attachment_id: this.attachment_id,
        });        

        loading.dismiss();
      }, error => {
        loading.dismiss();
      }
    );
  }

  onSubmit() {
    if (this.myForm.valid) {
      let loading = this.loadingCtrl.create({
        content: 'Loading...'
      });  
      loading.present();
            
      this.orderHistoryProvider.getOrderHistory(this.order_number.value).subscribe(data => {        
        if (data['data']) {
          let order_id = data['data']['id'];
          let body = this.myForm.value;
          Object.assign(body, {"order_id": order_id, "status": 1});
          delete body.order_number;
          delete body.file;
          delete body.email;

          this.uploadProvider.confirmPayment(body).subscribe(
            data => {
              //console.log('data', data);
              this.navCtrl.setRoot(HomePage);
              loading.dismiss();
              this.presentToast();
            },
            err => {
              loading.dismiss();
              //console.log(err);
            }
          );
        } else {
          this.orderNumberError = 'Order Number Not Found';
          loading.dismiss();
        }
      });      
    } else {
      this.markFormGroupTouched(this.myForm);
    }
  }

  ionViewDidLoad() {
    this.utilsProvider.getBankList().subscribe(data => {
      if (data['data'].length > 0) {
        this.listBank = data['data'];
      }
    });    
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        control.controls.forEach(c => this.markFormGroupTouched(c));
      }
    });
  }  

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Thank You.",
      showCloseButton: true,
      duration: 5000
    });
    toast.present();
  }  

}
