import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ENV } from '@app/env';
import { CategoryService } from '../../services/category.service';
import { CategoryModel } from '../../models/category.model';
import { CataloglistPage } from '../cataloglist/cataloglist';

/**
 * Generated class for the CatalogtypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-catalogtype',
  templateUrl: 'catalogtype.html',
})
export class CatalogtypePage {

  results: string[];
  selectedCategory: CategoryModel[];
  httpStorage: string;  

  constructor(public navCtrl: NavController, public navParams: NavParams, public categoryService: CategoryService) {
    this.selectedCategory = navParams.get('category');   
    this.httpStorage = ENV.httpStorage;       
  }

  ionViewDidLoad() {
    this.getTypes();
    console.log('ionViewDidLoad CatalogtypePage');
  }

  getTypes(): void {
    const id = this.selectedCategory['id'];
    this.categoryService.getCatalogtypes(id)
        .subscribe(data => {
          this.results = data['data']
        }
    );  
  }  

  itemTapped(event, category) {
    this.navCtrl.push(CataloglistPage, {
      category: category
    });
    console.log(category);
  }   

}
