import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CartProvider } from '../../providers/cart/cart';
import { UtilsProvider } from '../../providers/utils/utils';
import { Device } from '@ionic-native/device';
import { AddcartModel } from '../../models/addcart.model';
import { OrderDetailPage } from '../../pages/order-detail/order-detail';
/**
 * Generated class for the CheckoutListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-checkout-list',
  templateUrl: 'checkout-list.html',
})
export class CheckoutListPage {

  results: any[];
  cartItems: any[];
  addressShipping: any[];
  addressBilling: any[];
  shipping: any[];
  listProvince: any[];
  listCity: any[];  
  listBank: any;
  formOrder: FormGroup; 
  errors = {"message": ""};

  public loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });  

  constructor(public navCtrl: NavController, public navParams: NavParams, public utilsProvider: UtilsProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, private cartProvider: CartProvider, public toastCtrl: ToastController, public device: Device) {
    this.formOrder = new FormGroup({
      'payment_id': new FormControl('', Validators.required),
      'terms': new FormControl('0', [Validators.required, Validators.min(1)])
    });       
  }

  get payment_id() { return this.formOrder.get('payment_id'); }  

  get terms() { return this.formOrder.get('terms'); }  

  ionViewDidLoad() {
    this.getCheckoutList();
    this.listBank = [{
      'id': 1,
      'name': 'Bank Transfer'
    }];
    // this.utilsProvider.getBankList().subscribe(data => {
    //   if (data['data'].length > 0) {
    //     this.listBank = data['data'];
    //   }
    // });         
  }

  getCheckoutList(): void{
    this.loading.present();
    this.cartProvider.getCheckoutList().subscribe(data => {  
      if (data['data']) {
        this.results = data['data'];   
        this.cartItems = data['data']['items']; 
        this.addressBilling = data['data']['address_billing'];
        this.addressShipping = data['data']['address_shipping'];
        this.shipping = data['data']['shipping'];
        this.getProvince(this.addressBilling['province_id']);
      } else { 
        this.showAlert();
      }
      this.loading.dismiss();
    });    
  }
  
  getProvince(provinceId) {
    this.utilsProvider.getProvince().subscribe(data => {
      if (data['data'].length > 0) {
        this.listProvince = data['data'];
      }
    });
    this.utilsProvider.getCity(provinceId).subscribe(data => {
      if (data['data'].length > 0) {
        this.listCity = data['data'];        
      }
    });           
  }

  filterCity(cityId): any {
    if (this.listCity) {
      let filterCity = this.listCity.filter(item => {
        if (item.id == cityId) {
          return item;
        }
      });
      let city = filterCity[0]['name'];
      return city;
    }
    return '';
  }

  filterProvince(provinceId): string {
    if (this.listProvince) {
      let filterProvince = this.listProvince.filter(item => {
        if (item.id == provinceId) {
          return item;
        }
      });
      let province = filterProvince[0]['name'];
      return province;
    }
    return provinceId;
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Alert!',
      subTitle: 'Empty shopping cart!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }  

  submit() {
    if (this.formOrder.valid) {
      let loading = this.loadingCtrl.create({
        content: 'Loading...'
      });  
      loading.present();
      let body = {
        "device_id": this.device.uuid,
        "user_id": localStorage.getItem('user_id'),
        "address_shipping_id" : localStorage.getItem('address_shipping_id'),
        "address_billing_id": localStorage.getItem('address_billing_id'),
        "shipping_id": localStorage.getItem('shipping_id'),
        "payment_id": this.payment_id.value,
      }
      this.cartProvider.checkoutOrder(body).subscribe(data => {
        
        if (data) {
          this.navCtrl.setRoot(OrderDetailPage, {
            order_number: data['data']['order_number']
          });
        }

        let toast = this.toastCtrl.create({
          message: "Order Success",
          showCloseButton: true,
          duration: 5000
        });
        loading.dismiss();
        toast.present();  
        
        this.cartProvider.getCartListByUui().subscribe(data => {
          if (data['data'].length > 0) {
            for (let item of data['data']) {
              let addCartModel = new AddcartModel(this.device.uuid, item['product_id'], item['quantity']);
              this.cartProvider.deleteCart(addCartModel).subscribe(() => console.log('Delete addtocart'));
            }
          }
        });        
        
      }, err => {
        loading.dismiss();
        this.errors.message = 'Server Error!';
      });
    } else {
      Object.keys(this.formOrder.controls).forEach(field => { 
        const control = this.formOrder.get(field);            
        control.markAsTouched({ onlySelf: true });      
      });    
    }
  }

}
