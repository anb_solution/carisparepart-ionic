import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, } from '@angular/forms';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { UtilsProvider } from '../../providers/utils/utils';
import { CartProvider } from '../../providers/cart/cart';
import { LoginPage } from '../../pages/login/login';
import { CheckoutListPage } from '../../pages/checkout-list/checkout-list';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the CheckoutShippingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-checkout-shipping',
  templateUrl: 'checkout-shipping.html',
})
export class CheckoutShippingPage {

  shippingForm: FormGroup;
  listAddress: any;
  listShippingMethods: any;
  listProvince: any;
  listCity: any;
  errors = {"message": ""}; 
 
  userData = {
    "isUseMyExistingAddress": "1",
    "email": "", 
    "use_existing_address": "", 
    "fullname": "", 
    "address": "", 
    "province_id": "", 
    "city_id": "",
    "postcode": "", 
    "phone": "", 
    "is_billing_address_same": "1", 
    "billing_fullname": "", 
    "billing_address": "", 
    "billing_province_id": "", 
    "billing_city_id": "", 
    "billing_postcode": "", 
    "billing_phone": "", 
    "shipping_id": ""
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public authServiceProvider: AuthServiceProvider, public utilsProvider: UtilsProvider, public cartProvider: CartProvider, public loadingCtrl: LoadingController, public device: Device ) {
    this.shippingForm = new FormGroup({
      "isUseMyExistingAddress": new FormControl(this.userData.isUseMyExistingAddress, Validators.required),
      "email": new FormControl(this.userData.email, [Validators.required, Validators.email]),
      'use_existing_address': new FormControl(this.userData.use_existing_address),
      'fullname': new FormControl(this.userData.fullname, Validators.required),
      'address': new FormControl(this.userData.address, Validators.required),
      'province_id': new FormControl(this.userData.province_id, Validators.required),
      'city_id': new FormControl(this.userData.city_id, Validators.required),
      'postcode': new FormControl(this.userData.postcode, Validators.required),
      'phone': new FormControl(this.userData.phone, Validators.required),
      'is_billing_address_same': new FormControl(this.userData.is_billing_address_same),
      'billing_fullname': new FormControl(this.userData.billing_fullname),
      'billing_address': new FormControl(this.userData.billing_address),
      'billing_province_id': new FormControl(this.userData.billing_province_id),
      'billing_city_id': new FormControl(this.userData.billing_city_id),      
      'billing_postcode': new FormControl(this.userData.billing_postcode),
      'billing_phone': new FormControl(this.userData.billing_phone),
      'shipping_id': new FormControl(this.userData.shipping_id, Validators.required),
    });      
  }

  ionViewDidLoad() {
    this.setAddress();
    this.utilsProvider.getShippingList().subscribe(data => {
      if (data['data'].length > 0) {
        this.listShippingMethods = data['data'];
        for (let i = 0; i < this.listShippingMethods.length ; i++) {
          if (i == 0) {
            this.shipping_id.reset(this.listShippingMethods[i]['id']);
          }
        }         
      }
    });

    this.utilsProvider.getProvince().subscribe(data => {
      if (data['data'].length > 0) {
        this.listProvince = data['data'];
      }
    });    

    if (this.authServiceProvider.isLoggedIn()) {
      this.userData.email = this.authServiceProvider.userData['email'];
      this.email.reset(this.userData.email);
    } 
    
  }  
  
  get isUseMyExistingAddress() { return this.shippingForm.get('isUseMyExistingAddress'); } 

  get email() { return this.shippingForm.get('email'); }

  get use_existing_address() { return this.shippingForm.get('use_existing_address'); }

  get fullname() { return this.shippingForm.get('fullname'); }

  get address() { return this.shippingForm.get('address'); }

  get province_id() { return this.shippingForm.get('province_id'); }

  get city_id() { return this.shippingForm.get('city_id'); }

  get postcode() { return this.shippingForm.get('postcode'); }

  get phone() { return this.shippingForm.get('phone'); }

  get is_billing_address_same() { return this.shippingForm.get('is_billing_address_same'); }

  get billing_fullname() { return this.shippingForm.get('billing_fullname'); }

  get billing_address() { return this.shippingForm.get('billing_address'); }

  get billing_province_id() { return this.shippingForm.get('billing_province_id'); }

  get billing_city_id() { return this.shippingForm.get('billing_city_id'); }

  get billing_postcode() { return this.shippingForm.get('billing_postcode'); }

  get billing_phone() { return this.shippingForm.get('billing_phone'); } 
  
  get shipping_id() { return this.shippingForm.get('shipping_id'); } 


  setAddress() {
    if (this.authServiceProvider.isLoggedIn()) {
      this.authServiceProvider.getAddress().subscribe(data => {
        if (data['data'].length > 0) {
          this.listAddress = data['data'];
          for (let i = 0; i < this.listAddress.length ; i++) {
            if (i == 0) {
              this.use_existing_address.reset(this.listAddress[i]['id']);
              this.selectedExistingAddress(this.listAddress[i]['id']);
            }
          }       
        } else {
          this.userData.isUseMyExistingAddress = '0';
          this.isUseMyExistingAddress.reset(0);
          this.use_existing_address.reset(0);
        }      
      });    
    } else {
      this.userData.isUseMyExistingAddress = '0';
      this.isUseMyExistingAddress.reset(0);
      this.use_existing_address.reset(0);
    }
  }
  
  selectedExistingAddress(event) {
    let filteredAddress = this.listAddress.filter(item => {
      if (item.id == event) {
        return item;
      }
    });
    let address = filteredAddress[0];
    if (address) {
      this.fullname.reset(address['full_name']);
      this.address.reset(address['address']);
      this.province_id.reset(address['province_id']);
      this.city_id.reset(address['city_id']);   
      this.postcode.reset(address['postcode'] ? address['postcode'] : ' ');
      this.phone.reset(address['phone']);    

      this.billing_fullname.reset(address['full_name']);
      this.billing_address.reset(address['address']);
      this.billing_province_id.reset(address['province_id']);
      this.billing_city_id.reset(address['city_id']);   
      this.billing_postcode.reset(address['postcode'] ? address['postcode'] : ' ');
      this.billing_phone.reset(address['phone']);        
      
      this.provinceSelect(address['province_id']);
    }
  }

  setBillingAddress() {
    this.billing_fullname.reset(this.fullname.value);
    this.billing_address.reset(this.address.value);
    this.billing_province_id.reset(this.province_id.value);
    this.billing_city_id.reset(this.city_id.value);   
    this.billing_postcode.reset(this.postcode.value);
    this.billing_phone.reset(this.phone.value);       
  }

  provinceSelect(event) {
    this.utilsProvider.getCity(event).subscribe(data => {
      if (data['data'].length > 0) {
        this.listCity = data['data'];
      }
    });     
  }


  checkExistingAddress() {
    this.userData.isUseMyExistingAddress = this.isUseMyExistingAddress.value;
    if (this.userData.isUseMyExistingAddress == '0') {
      this.use_existing_address.reset(0);
    } else if (this.userData.isUseMyExistingAddress == '1'){
      this.setAddress();
    }
  }

  autoRegister() {

  }

  submit() {
    this.setBillingAddress();
    let body = this.shippingForm.value;
    delete body['isUseMyExistingAddress'];
    
    if (this.shippingForm.valid) {

      let loading = this.loadingCtrl.create({
        content: 'Loading...'
      });  
      loading.present();

      if (!Number.isInteger(body.postcode)) {
        body.postcode = 0;
      }
      if (!Number.isInteger(body.billing_postcode)) {
        body.billing_postcode = 0;
      }
      this.cartProvider.addShippginAddress(body).subscribe(data => {
        //console.log('submit', data); 
        this.navCtrl.push(CheckoutListPage);
        loading.dismiss();
      }, err => {   
        loading.dismiss();     
        if (err['error']['message'] == 'email not found' && !this.authServiceProvider.isLoggedIn()) {
          this.authServiceProvider.register({
            "first_name": this.fullname.value, 
            "last_name": "", 
            "telp": "", 
            "phone": this.phone.value, 
            "email": this.email.value, 
            "password": Math.random().toString(36).substring(7), 
            "repeatPassword": ""
          }, false).subscribe(data => {
            this.submit();        
          }, errReg => {
            this.errors.message = err['error']['message'];
          });
        } else {
          this.errors.message = err['error']['message'];
        }        
      });   
    } else {
      Object.keys(this.shippingForm.controls).forEach(field => { 
        const control = this.shippingForm.get(field);            
        control.markAsTouched({ onlySelf: true });      
      });
    }
  }

  loginPage() {
    this.navCtrl.push(LoginPage);
  }
}
