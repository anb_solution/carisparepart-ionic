import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, ViewController } from 'ionic-angular';
import { ENV } from '@app/env';
import { CategoryService } from '../../services/category.service';
import { CategoryModel } from '../../models/category.model';
import { CatalogtypePage } from '../catalogtype/catalogtype';
import { ProductProvider } from '../../providers/product/product';
import { ProductPage } from '../product/product';

/**
 * Generated class for the ListmodelsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-listmodels',
  templateUrl: 'listmodels.html',
})
export class ListmodelsPage {

  cat: string = "all";
  query = {"no": ""};
  results: string[];
  selectedCategory: CategoryModel[];
  httpStorage: string;
  items: any[];
  errors = {"message": ""};

  constructor(public navCtrl: NavController, public navParams: NavParams, private productProvider: ProductProvider, private categoryService: CategoryService, public alertCtrl: AlertController, public modalCtrl: ModalController) {
    this.selectedCategory = navParams.get('category');    
    this.httpStorage = ENV.httpStorage;    
    this.getCategories();
    this.initializeItems();
  }

  ionViewDidLoad() {
    
    console.log('ionViewDidLoad ListmodelsPagex');
  }

  ionViewDidEnter() {
    if (this.results.length === 0) {
      let alert = this.alertCtrl.create({
        title: this.selectedCategory['name'],
        subTitle: 'Part Not Available',
        buttons: [{
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
          }
        }]
      });
      alert.present();
    }
  }

  getCategories(): void {
    const id = this.selectedCategory['id'];
    this.categoryService.getCategoriesByParent(id)
        .subscribe(data => {
          let datas = data['data'];
          datas.filter( (item) => { if ((item.name).charAt(0) == ' ') { return item.name = item.name.indexOf(' ') == 0 ? item.name.substring(1) : item.name; } return item   } )
          datas.sort(function(a, b) {
            var nameA = a.name.toUpperCase(); // ignore upper and lowercase
            var nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
          
            // names must be equal
            return 0;
          });          
          this.results = datas;
          this.items = this.results;
        }
    );  
  }
  initializeItems() {
    this.items = this.results;
  }

  presentFilterModal() {
    console.log('presentFilterModal');
    let filterModal = this.modalCtrl.create(Filter, this.selectedCategory);
    filterModal.onDidDismiss(data => {
      console.log(data);
    });
    filterModal.present();
  }

  getItems(event: any) {
    this.initializeItems();
    let val = event.target.value;
    if (val && val.trim() != '') {
      this.items = this.results.filter((item) => {
        return (item['name'].toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  itemTapped(event, category) {
    this.navCtrl.push(CatalogtypePage, {
      category: category
    });
  }    

  searchPartNo() {
    this.productProvider.getProductsByNumber(this.query.no).subscribe(result => {
      if (!result.data || result.data.length <= 0) {
        this.errors.message = "Part Number Not Found.";
        console.log("err");
      } else if(result.data) {
        this.navCtrl.push(ProductPage, {
          selectedPart: result.data[0]
        });         
      }
    });    
  }

}

@Component({
  selector: 'page-filter-category',
  templateUrl: 'filter.html',
})
export class Filter {

  constructor(params: NavParams, public viewCtrl: ViewController) {
    console.log('name', params.get('name'));
  }

  dismiss() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

}