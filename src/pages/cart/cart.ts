import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ModalController, ViewController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';
import { AddcartModel } from '../../models/addcart.model';
import { CheckoutShippingPage } from '../checkout-shipping/checkout-shipping';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  results: any[];

  public loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });  

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private cartProvider: CartProvider, private device: Device, public alertCtrl: AlertController, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    this.getCartList();    
  }

  getCartList(): void{
    this.loading.present();
    this.cartProvider.getCartList().subscribe(data => {  
      if (data['data'].length > 0) {
        this.results = data['data'];     
        this.sumPrice();
      } else {
        this.showAlert();
      }
      this.loading.dismiss();
    });    
    console.log(this.device.uuid);
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Alert!',
      subTitle: 'Empty shopping cart!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  updateModal(item: AddcartModel) {
    let updateModal = this.modalCtrl.create(UpdateCartPage, { item: item });
    updateModal.present();
  } 

  sumPrice() : number {
    let nPrice = 0;
    let nQuantity = 0;
    for (let value of this.results) {
      let totalPrice = value['quantity'] * value['Product']['price'];
      nQuantity += value['quantity'];
      nPrice += totalPrice;
    }
    return nPrice;
  }

  checkout() {
    this.navCtrl.push(CheckoutShippingPage);
  }

  delete(item: AddcartModel): void {
    let confirm = this.alertCtrl.create({
      title: 'Delete item?',
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Ok',
          handler: () => {
            item.device_id = this.device.uuid;
            this.results = this.results.filter(r => r !== item);
            this.cartProvider.deleteCart(item).subscribe(() => {
              this.sumPrice();
            });
          }
        }
      ]
    });
    confirm.present();  
  }
}


@Component({
  selector: 'page-update-cart',
  templateUrl: 'update-cart.html',
})
export class UpdateCartPage {
  item: any[];
  cartUpdated: AddcartModel;
   
  constructor(params: NavParams, public viewCtrl: ViewController, public device: Device, public cartProvider: CartProvider) {
    this.item = params.get('item');
  }  

  updateCart() {
    this.cartUpdated = new AddcartModel(this.device.uuid, this.item['product_id'], this.item['quantity']);
    this.cartProvider.updateCart(this.cartUpdated).subscribe(() => { console.log('cart updated') });
    this.viewCtrl.dismiss();    
  }

  range(start: number, end: number): Array<number> {
    let results = [];
    for (let i = start; i <= end; i++) { 
      results.push(i);
    }
    return results;
  }   

  dismiss() {
    this.viewCtrl.dismiss();
  }  
}