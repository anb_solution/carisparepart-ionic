import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { CartProvider } from '../../providers/cart/cart';
import { HomePage } from '../home/home';
import { AddcartModel } from '../../models/addcart.model';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  responseData : any;
  userData = {"email": "", "password": ""};
  errors = {"message": ""};
  map: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authServiceProvider: AuthServiceProvider, public loadingCtrl: LoadingController, public cartProvider: CartProvider, private device: Device) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {    
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.authServiceProvider.login(this.userData).subscribe(data => {
      this.responseData = data;
      loader.dismiss();
      if (this.responseData['data']) {
        
        this.cartProvider.getCartListByUui().subscribe(data => {
          if (data['data'].length > 0) {
            for (let item of data['data']) {
              let addCartModel = new AddcartModel(this.device.uuid, item['product_id'], item['quantity']);
              this.cartProvider.addToCart(addCartModel).subscribe(() => console.log('Login and addtocart'));
            }
          }
        });

        this.cartProvider.setTotalItem();
        localStorage.setItem('userData', JSON.stringify(this.responseData['data']));
        this.navCtrl.setRoot(HomePage);
      } else {
        this.errors.message = 'Login Error!';
      }
    }, err => {
      if (err['error']['message']) { 
        this.errors.message = err['error']['message'];
      } else {
        this.errors.message = 'Login Error!';
      }
      loader.dismiss();
    });    
  }

}
