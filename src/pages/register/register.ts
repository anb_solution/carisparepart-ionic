import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { CartProvider } from '../../providers/cart/cart';
import { HomePage } from '../home/home';
import { AddcartModel } from '../../models/addcart.model';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  registerForm: FormGroup;
  responseData : any;
  userData = {"first_name": "", "last_name": "", "telp": "", "phone": "", "email": "", "password": "", "repeatPassword": ""};
  errors = {"message": ""};

  constructor(public navCtrl: NavController, public navParams: NavParams, public authServiceProvider: AuthServiceProvider, public loadingCtrl: LoadingController, public cartProvider: CartProvider, public device: Device) {

    this.registerForm = new FormGroup({
      'first_name': new FormControl(this.userData.first_name, Validators.required),
      'last_name': new FormControl(this.userData.last_name, Validators.required),
      'telp': new FormControl(this.userData.telp),
      'phone': new FormControl(this.userData.phone, Validators.required),
      'email': new FormControl(this.userData.email, [Validators.required, Validators.email]),
      'password': new FormControl(this.userData.password, [Validators.required, Validators.minLength(4)]),            
    });   

  } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage'); 
  }

  get first_name() { return this.registerForm.get('first_name'); }

  get last_name() { return this.registerForm.get('last_name'); }  

  get telp() { return this.registerForm.get('telp'); }

  get phone() { return this.registerForm.get('phone'); }  

  get email() { return this.registerForm.get('email'); }

  get password() { return this.registerForm.get('password'); }    

  register() {
    if (this.registerForm.status == 'VALID') {

      this.userData = this.registerForm.value;
      let loader = this.loadingCtrl.create({
        content: "Please wait...",
      });
      loader.present();
      
      this.authServiceProvider.register(this.userData).subscribe(data => {
        this.responseData = data;
        loader.dismiss();
        if (this.responseData['data']) {
          this.cartProvider.getCartListByUui().subscribe(data => {
            if (data['data'].length > 0) {
              for (let item of data['data']) {
                let addCartModel = new AddcartModel(this.device.uuid, item['product_id'], item['quantity']);
                this.cartProvider.addToCart(addCartModel).subscribe(() => console.log('Login and addtocart'));
              }
            }
          });          
          this.navCtrl.setRoot(HomePage);
        } else {
          this.errors.message = 'Login Error!';
        }
      }, err => {
        if (err['error']['message']) { 
          this.errors.message = err['error']['message'];
        } else {
          this.errors.message = 'Register Error!';
        }
        loader.dismiss();
      });   
    }
  }

}
