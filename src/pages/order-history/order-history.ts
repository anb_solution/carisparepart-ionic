import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OrderHistoryProvider } from '../../providers/order-history/order-history';
import { OrderDetailPage } from '../../pages/order-detail/order-detail';
/**
 * Generated class for the OrderHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-order-history',
  templateUrl: 'order-history.html',
})
export class OrderHistoryPage {

  results: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public orderHistoryProvider: OrderHistoryProvider) {
  }

  ionViewDidLoad() {
    this.getOrderHistories();
  }

  getOrderHistories() {
    this.orderHistoryProvider.getOrderHistories().subscribe(data => {
      this.results = data['data'];
    });
  }

  itemTapped(event, item) { 
    this.navCtrl.push(OrderDetailPage, {
      order_number: item
    });    
  }
  
  getStatus(status) {
    return this.orderHistoryProvider.status[status];
  }

  getDateTime(time) {
    let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    let date = new Date(time*1000);
    let days:any = date.getDate();
    let hours:any = date.getHours();
    let minutes:any = date.getMinutes();
    if (date.getDate() <= 9) {
      days = `0${date.getDate()}`;
    }
    if (date.getMinutes() <= 9) {
      minutes = `0${date.getMinutes()}`;
    }    
    if (date.getHours() <= 9) {
      hours = `0${date.getHours()}`;
    }  
    return `${days} ${months[date.getMonth()]} ${date.getFullYear()} - ${hours}:${minutes}`;
  }

}
