import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OrderHistoryProvider } from '../../providers/order-history/order-history';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConfirmationPaymentPage } from '../../pages/confirmation-payment/confirmation-payment';
/**
 * Generated class for the OrderDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {

  order: any;
  //item: any;
  order_number: any;
  listProvince: any[];
  listCity: any[];    
  totalWeight: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public orderHistoryProvider: OrderHistoryProvider, public utilsProvider: UtilsProvider) {
    this.order_number = navParams.get('order_number');
 
  }

  ionViewDidLoad() {
    this.getOrderHistory(this.order_number);
  }

  getOrderHistory(order_number) {
    this.orderHistoryProvider.getOrderHistory(order_number).subscribe(data => {
      this.order = data['data'];
      this.getProvince(this.order['OrderDelivery']['province_id']);
    }); 
  }

  getProvince(provinceId) {
    this.utilsProvider.getProvince().subscribe(data => {
      if (data['data'].length > 0) {
        this.listProvince = data['data'];
      }
    });
    this.utilsProvider.getCity(provinceId).subscribe(data => {
      if (data['data'].length > 0) {
        this.listCity = data['data'];        
      }
    });           
  }  

  filterCity(cityId): any {
    if (this.listCity) {
      let filterCity = this.listCity.filter(item => {
        if (item.id == cityId) {
          return item;
        }
      });
      //console.log('cities', this.listCity);
      let city = filterCity[0]['name'];
      return city;
    }
    return '';
  }
  
  getStatus(status) {
    return this.orderHistoryProvider.status[status];
  }

  filterProvince(provinceId): string {
    if (this.listProvince) {
      let filterProvince = this.listProvince.filter(item => {
        if (item.id == provinceId) {
          return item;
        }
      });
      let province = filterProvince[0]['name'];
      return province;
    }
    return provinceId;
  }  

  confirmPayment(number: number) {
    this.navCtrl.push(ConfirmationPaymentPage, {
      order_number: number
    });      
  }

}
