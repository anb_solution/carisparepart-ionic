import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ENV } from '@app/env';
import { CategoryService } from '../../services/category.service';
import { ListmodelsPage } from '../listmodels/listmodels';
import { CategoryModel } from '../../models/category.model';


/**
 * Generated class for the ListbrandsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-listbrands',
  templateUrl: 'listbrands.html',
})
export class ListbrandsPage {

  results: string[];
  selectedCategory: CategoryModel[];
  httpStorage: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private categoryService: CategoryService) {
    this.selectedCategory = navParams.get('category');    
    this.httpStorage = ENV.httpStorage;
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ListbrandsPage');
    this.getCategories();
  }

  getCategories(): void {
    const id = this.selectedCategory['id'];
    this.categoryService.getCategoriesByParent(id)
        .subscribe(data => {
          this.results = data['data']
        });    
  }

  itemTapped(event, category) {
    this.navCtrl.push(ListmodelsPage, {
      category: category
    });
    console.log(category);
  }  

}
