import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CatalogProvider } from '../../providers/catalog/catalog';
import { ProductPage } from '../product/product';
import { ENV } from '@app/env';

/**
 * Generated class for the CatalogDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-catalog-detail',
  templateUrl: 'catalog-detail.html',
})
export class CatalogDetailPage {

  isGroup: boolean = false;
  catalogProducts: string[];
  catalogMappings: string[];
  catalog: string[];
  httpStorage: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public catalogProvider: CatalogProvider) {
    this.catalog = navParams.get('catalog');     
    this.httpStorage = ENV.httpStorage;  
  }

  ionViewDidLoad() {
    this.getProducts();
    this.getMapping();    
  }

  getProducts() { 
    const catalog_id = this.catalog['id'];
    this.catalogProvider.getProducts(catalog_id)
        .subscribe(data => {
          this.catalogProducts = data['data'];
        }
    );    
  }  

  getMapping() {
    const catalog_id = this.catalog['id'];
    this.catalogProvider.getMapping(catalog_id)
        .subscribe(data => {
          this.catalogMappings = data['data'];       
        }
    );    
  }

  itemTapped(event, item) {
    this.navCtrl.push(ProductPage, {
      selectedPart: item
    }); 
  }

  itemGroupTapped(event, item) {
    this.navCtrl.push(ProductPage, {
      selectedPart: item.Product
    }); 
  }

  toggleSection(i) {
    this.catalogProducts[i]['open'] = !this.catalogProducts[i]['open'];
  }
 
  toggleItem(i, j) {
    this.catalogProducts[i]['CatalogProductGroupItems'][j]['open'] = !this.catalogProducts[i]['CatalogProductGroupItems'][j]['open'];
  }  

}
