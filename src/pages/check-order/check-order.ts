import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OrderHistoryProvider } from '../../providers/order-history/order-history';
import { OrderDetailPage } from '../../pages/order-detail/order-detail';

/**
 * Generated class for the CheckOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-check-order',
  templateUrl: 'check-order.html',
})
export class CheckOrderPage {
  orderNumber: number;
  errors = {"message": ""};

  constructor(public navCtrl: NavController, public navParams: NavParams, public orderHistoryProvider: OrderHistoryProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckOrderPage');
  }

  check() {

    this.orderHistoryProvider.getOrderHistory(this.orderNumber).subscribe(data => {
      let order = data['data'];
      if (order) {
        console.log(data);
        this.navCtrl.push(OrderDetailPage, {
          order_number: order['order_number']
        });          
      } else {
        this.errors.message = 'Not Found';
      }
    }, err => {
      this.errors.message = 'Not Found';
    });     
  }

}
