import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';

import { ENV } from '@app/env';
//import { CategoryModel } from '../../models/category.model';
import { CategoryService } from '../../services/category.service';
import { ListbrandsPage } from '../listbrands/listbrands';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  results: string[];
  title: string;
  httpStorage: string;

  public loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });  

  constructor(public navCtrl: NavController, private categoryService: CategoryService, public loadingCtrl: LoadingController, public authServiceProvider : AuthServiceProvider, public toastCtrl: ToastController) {
    this.title = ENV.mode;
    this.httpStorage = ENV.httpStorage;
  }

  ionViewDidLoad() {
    this.getCategories();
  }
  
  getCategories(): void {
    this.loading.present();
    this.categoryService.getCategories()
        .subscribe(data => {
          this.results = data['data'];
          this.loading.dismiss();
        });
  }  

  itemTapped(event, category) {
    this.navCtrl.push(ListbrandsPage, {
      category: category
    });
    console.log(category);
  }  

}
