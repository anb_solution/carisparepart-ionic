import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';
import { Product } from '../../models/product.model';
import { AddcartModel } from '../../models/addcart.model';
import { Device } from '@ionic-native/device';
import { ENV } from '@app/env';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  selectedPart: string;
  product: Product;
  products: Product[];
  httpStorage: string = ENV.httpStorage;
  addCartModel: AddcartModel = new AddcartModel(this.device.uuid, 0, 0);

  constructor(public navCtrl: NavController, public navParams: NavParams, private productProvider: ProductProvider, private cartProvider: CartProvider, public alertCtrl: AlertController, public toastCtrl: ToastController, private device: Device) {
    this.selectedPart = navParams.get('selectedPart');    
  }
  
  ionViewDidLoad() {              
    this.getProduct();  
    this.getProductsByNumber();
  }

  getProduct(): void {
    let product_id = this.selectedPart['id'];    
    this.productProvider.getProduct(product_id).subscribe(result => {
      this.product = result.data;
    });
  }

  getProductsByNumber(): void {
    let part_number = this.selectedPart['no_part']; 
    this.productProvider.getProductsByNumber(part_number).subscribe(result => {
      this.products = result.data;  
    });    
  }

  onSubmit(productId: number) {
    this.addCartModel.product_id = productId;    
    if (!this.addCartModel.quantity) {
      let alert = this.alertCtrl.create({
        title: 'Error!',
        subTitle: 'Please select quantity',
        buttons: ['OK']
      });
      alert.present();      
    } 
    if (this.addCartModel.quantity) {
      this.cartProvider.addToCart(this.addCartModel).subscribe(() => {
        this.presentToast();
      });
    }
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "You've just put this item into your cart.",
      showCloseButton: true,
      duration: 5000
    });
    toast.present();
  }

  range(start: number, end: number): Array<number> {
    let results = [];
    for (let i = start; i <= end; i++) { 
      results.push(i);
    }
    return results;
  }

}
