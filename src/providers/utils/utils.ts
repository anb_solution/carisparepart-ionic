import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { ENV } from '@app/env';

@Injectable()
export class UtilsProvider {
    
    private shippingListUrl = `${ENV.httpEndPoint}shipping/list`;
    private provinceUrl = `${ENV.httpEndPoint}geograph/province/97`;
    private cityUrl = `${ENV.httpEndPoint}geograph/city`;
    private bankListUrl = `${ENV.httpEndPoint}bank/list`;
    
    constructor(public http: HttpClient) {
    }
    
    private log(message: string) {
        console.log('CatalogProvider: ' + message);
    } 
    
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
    };
    }  

    getShippingList(): Observable<string[]> {
        const url = this.shippingListUrl;
        return this.http.get<any[]>(url)
        .pipe(
            tap(shipping => this.log(`fetched shipping`)),
            catchError(this.handleError('getShippingList', []))
        );
    }

    getBankList(): Observable<string[]> {
        const url = this.bankListUrl;
        return this.http.get<any[]>(url)
        .pipe(
            tap(shipping => this.log(`fetched getBankList`)),
            catchError(this.handleError('getBankList', []))
        );
    }    

    getProvince(): Observable<string[]> {
        const url = this.provinceUrl;
        return this.http.get<any[]>(url)
        .pipe(
            tap(shipping => this.log(`fetched province`)),
            catchError(this.handleError('getProvince', []))
        );
    }    

    getCity(province_id: number): Observable<string[]> {
        const url = `${this.cityUrl}/${province_id}`;;
        return this.http.get<any[]>(url)
        .pipe(
            tap(shipping => this.log(`fetched province`)),
            catchError(this.handleError('getProvince', []))
        );
    }        
}