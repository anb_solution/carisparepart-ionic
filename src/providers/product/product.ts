import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { ENV } from '@app/env';

/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductProvider {

  private productUrl = `${ENV.httpEndPoint}product`;
  private productByNumberUrl = `${ENV.httpEndPoint}product/number`;

  constructor(public http: HttpClient) {
    console.log('Hello ProductProvider Provider');
  }

  private log(message: string) {
    console.log('ProductProvider: ' + message);
  } 

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
    };
  }
  

  getProduct(product_id: number): Observable<any> {
    const url = `${this.productUrl}/${product_id}`;
    return this.http.get<any>(url)    
    .pipe(
        tap(product => this.log(`fetched product ${product_id}`)),
        catchError(this.handleError(`getProduct ${product_id}`, []))
    );
  }

  getProductsByNumber(no_part: string): Observable<any> {
    const url = `${this.productByNumberUrl}/${no_part}`;
    return this.http.get<any>(url)    
    .pipe(
        tap(product => this.log(`fetched producst ${no_part}`)),
        catchError(this.handleError(`getProductsByNumber ${no_part}`, []))
    );    
  }

}
