import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { ENV } from '@app/env';

/*
  Generated class for the CatalogProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CatalogProvider {

  private catalogsUrl = ENV.httpEndPoint + 'catalog/type'; 
  private catalogDetailUrl = ENV.httpEndPoint + 'catalog/detail';
  private catalogMappingUrl = ENV.httpEndPoint + 'catalog/mapping';
  private catalogProducts = ENV.httpEndPoint + 'catalog/product';

  constructor(public http: HttpClient) {
    console.log('Hello CatalogProvider Provider');
  }

  private log(message: string) {
    console.log('CatalogProvider: ' + message);
  } 

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
    };
  }     

  getCatalogs(category_id: number, type_id: number): Observable<string[]> {
    const url = `${this.catalogsUrl}/${category_id}/${type_id}`;
    return this.http.get<any[]>(url)
    .pipe(
        tap(catalogs => this.log(`fetched catalogs`)),
        catchError(this.handleError('getCatalogs', []))
    );        
  }

  getCatalog(catalog_id: number): Observable<string[]> {
    const url = `${this.catalogDetailUrl}/${catalog_id}`;
    return this.http.get<any[]>(url)
    .pipe(
        tap(catalog => this.log(`fetched catalog`)),
        catchError(this.handleError('getCatalog', []))
    );
  }

  getProducts(catalog_id: number) {
    const url = `${this.catalogProducts}/${catalog_id}`;
    return this.http.get(url)
    .pipe(
        tap(catalog => this.log(`fetched catalog`)),
        catchError(this.handleError('getCatalog', []))
    );
  }

  getMapping(catalog_id: number) {
    const url = `${this.catalogMappingUrl}/${catalog_id}`;
    return this.http.get(url)
    .pipe(
        tap(catalog => this.log(`fetched catalog`)),
        catchError(this.handleError('getCatalog', []))
    );    
  }

}
