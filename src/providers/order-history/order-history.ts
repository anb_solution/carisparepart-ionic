import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { ENV } from '@app/env';
import { AuthServiceProvider } from '../auth-service/auth-service';

/*
  Generated class for the OrderHistoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderHistoryProvider {

  private orderHistoryUrl = `${ENV.httpEndPoint}user/order/history`;

  public status = {'1': 'Pending', '2': 'Processing', '3': 'Shipped', '4': 'Completed', '5': 'Ask For Cancel', '6': 'Cancelled'};

  constructor(public http: HttpClient, public authServiceProvider : AuthServiceProvider) {
    console.log('Hello OrderHistoryProvider Provider');
  }

  private log(message: string) {
    console.log('OrderHistoryProvider: ' + message);
  } 

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
    };
  } 


  getOrderHistories(): Observable<string[]> {
    const url = `${this.orderHistoryUrl}`;
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');

    return this.http.get<any[]>(url, {headers: httpHeader})
    .pipe(
        tap(catalogs => this.log(`fetched order histories`)),
        catchError(this.handleError('getOrderHistories', []))
    );        
  } 

  getOrderHistory(order_number: number): Observable<string[]> {
    const url = `${this.orderHistoryUrl}/${order_number}`;
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');

    return this.http.get<any[]>(url, {headers: httpHeader})
    .pipe(
        tap(catalogs => this.log(`fetched order histories`)),
        catchError(this.handleError('getOrderHistories', []))
    );        
  }  

}
