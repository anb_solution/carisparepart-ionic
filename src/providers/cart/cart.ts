import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { AddcartModel } from '../../models/addcart.model';
import { AuthServiceProvider } from '../auth-service/auth-service';
import { ENV } from '@app/env';
import { Device } from '@ionic-native/device';

/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CartProvider {

  private addToCartUrl = `${ENV.httpEndPoint}cart/add`;
  private updateCartUrl = `${ENV.httpEndPoint}cart/update`; 
  private cartListUrl = `${ENV.httpEndPoint}cart/list`;
  private deleteCartUrl = `${ENV.httpEndPoint}cart/delete`;
  private addShippingAddressUrl = `${ENV.httpEndPoint}checkout/shipping-address`;
  private checkoutListUrl = `${ENV.httpEndPoint}checkout/list`;
  private checkoutOrderUrl = `${ENV.httpEndPoint}checkout/order`;

  totalItem: number = 0;

  constructor(public http: HttpClient, public authServiceProvider : AuthServiceProvider, private device: Device) {
    this.setTotalItem();
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  setTotalItem() {
    this.getCartList().subscribe(data => {
      let total = data['data'].length;  
      this.totalItem = total;
    });    
  }

  getCartList(): Observable<any[]> {
    let url = `${this.cartListUrl}/${this.device.uuid}`;
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');
    return this.http.get<any[]>(url, {headers: httpHeader})
      .pipe( 
        tap(lists => console.log(`fetched cart list`)),
        catchError(this.handleError('getCartList', []))
      );
  }

  getCartListByUui() {
    let url = `${this.cartListUrl}/${this.device.uuid}`;
    let httpHeader = new HttpHeaders().set('Content-Type', 'application/json');
    
    return this.http.get<any[]>(url, {headers: httpHeader})
      .pipe( 
        tap(lists => console.log(`fetched cart list by UUID`)),
        catchError(this.handleError('getCartListByUui', []))
      );    
  }

  deleteCart (item: AddcartModel): Observable<AddcartModel> {
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');
    return this.http.post(this.deleteCartUrl, item, {headers: httpHeader}).pipe(
      tap((item: AddcartModel) => { 
        this.setTotalItem();
      }),
      catchError(this.handleError<AddcartModel>('deleteHero'))
    );
  }  

  updateCart (item: AddcartModel): Observable<AddcartModel> {
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');
    return this.http.post<AddcartModel>(this.updateCartUrl, item, {headers: httpHeader}).pipe(
      tap((item: AddcartModel) => { 
        this.setTotalItem();
      }),
      catchError(this.handleError<AddcartModel>('addToCart'))
    );    
  }

  addToCart (item: AddcartModel): Observable<AddcartModel> {    
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');

    return this.http.post<AddcartModel>(this.addToCartUrl, item, {headers: httpHeader}).pipe(
      tap((item: AddcartModel) => { 
        this.setTotalItem();
      }),
      catchError(this.handleError<AddcartModel>('addToCart'))
    );
  }
  

  getCheckoutList(): Observable<any[]> {
    let body = {
      'device_id': this.device.uuid,
      'address_shipping_id': localStorage.getItem('address_shipping_id'),
      'address_billing_id': localStorage.getItem('address_billing_id'),
      'shipping_id': localStorage.getItem('shipping_id'),
    };
    let url = `${this.checkoutListUrl}`;
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');
    return this.http.post<any[]>(url, body, {headers: httpHeader})
      .pipe(
        tap(lists => {
          if (!this.authServiceProvider.isLoggedIn()) { 
            localStorage.setItem('user_id', lists['data']['address_billing']['user_id']); 
          }
        }),
        catchError(this.handleError('getCheckoutList', []))
      );
  }

  addShippginAddress(body: any): Observable<any> {    
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');

    return this.http.post<any>(this.addShippingAddressUrl, body, {headers: httpHeader}).pipe(
      tap((data) => {  
        localStorage.setItem('address_billing_id', data['data']['address_billing_id']); 
        localStorage.setItem('address_shipping_id', data['data']['address_shipping_id']); 
        localStorage.setItem('shipping_id', data['data']['shipping_id']);  
      })
    );
  }

  checkoutOrder(body: any): Observable<any> {    
    let httpHeader = new HttpHeaders().set('Authorization', this.authServiceProvider.getAuthorizationHeader());
    httpHeader.set('Content-Type', 'application/json');

    return this.http.post<any>(this.checkoutOrderUrl, body, {headers: httpHeader}).pipe(
      tap((order) => { 
        this.setTotalItem();
      }),
      catchError(this.handleError<any>('checkoutOrder'))
    );
  }

}
