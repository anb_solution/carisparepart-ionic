import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ENV } from '@app/env';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

// const httpOptions = {
//   headers: new HttpHeaders()
// };

@Injectable()
export class AuthServiceProvider {

  private loginUrl = ENV.httpEndPoint + 'auth/login';
  private registerUrl = ENV.httpEndPoint + 'auth/register';
  private profileUrl = ENV.httpEndPoint + 'user/profile';
  private addressUrl = ENV.httpEndPoint + 'user/address/shipping';
  
  userData: string;

  constructor(public http: HttpClient) {
    this.getProfile();
  }

  login(body) {
    return this.http.post(this.loginUrl, body, {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
    }).map((res) => {
      if (res['data']) {                
        localStorage.setItem('auth_token', res['data']['token']);
        localStorage.setItem('email', body['email']);  
      }
      this.getProfile();
      return res;
    });
  }


  register(body, isAutoLogin = true) {
    return this.http.post(this.registerUrl, body, {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
    }).map((res) => {
      if (res['data']) {              
        let loginData = {"email": body['email'], "password": body['password']};  
        if (isAutoLogin) { 
          this.login(loginData).subscribe();
        } else {
          localStorage.setItem('user_id', res['data']['user_id']); 
        }
      }
      return res;
    });
  }

  getToken() {
    let authToken = localStorage.getItem('auth_token');  
    return authToken;
  }

  getProfile() {
    if (!this.isLoggedIn()) {
      return false;
    }
    let httpHeader = new HttpHeaders().set('Authorization', this.getAuthorizationHeader());
    return this.http.get(this.profileUrl, {headers: httpHeader}).map((res) => {
      return res['data'];
    }).subscribe(data => {
      this.userData = data;
      localStorage.setItem('user_id', data['user_id']); 
      Object.assign(this.userData, {"email": localStorage.getItem('email')});
    }, err => {
      this.logout();
    });
  }

  getAddress() {
    let httpHeader = new HttpHeaders().set('Authorization', this.getAuthorizationHeader());
    return this.http.get(this.addressUrl, {headers: httpHeader});
  }

  getAuthorizationHeader() {
    return `Bearer ${this.getToken()}`;
  }

  logout() {
    localStorage.clear();
  }

  isLoggedIn() {
    let authToken = localStorage.getItem('auth_token');  
    if (authToken) {
      return true;
    }
    return false;
  }
  
    
}
