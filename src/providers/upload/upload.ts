import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';
import { ENV } from '@app/env';

/*
  Generated class for the UploadProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UploadProvider {

  private uploadUrl = `${ENV.httpEndPoint}upload/image`;
  private confirmPaymentUrl = `${ENV.httpEndPoint}checkout/payment-confirmation`;

  constructor(public http: HttpClient) {
    console.log('Hello UploadProvider Provider');
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }  

  postFile(fileToUpload: File, extraFields: any): Observable<any> {
    const url = this.uploadUrl;
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    for (let extraField of extraFields) {
      formData.append(extraField['name'], extraField['value']);
    }
    //let httpHeader = new HttpHeaders().append('Content-Type', 'multipart/form-data');
    //httpHeader.append('Accept', 'application/json');

    return this.http.post(url, formData, {headers: new HttpHeaders()})
    .pipe( 
      tap(lists => console.log(`postFile taped`)),
      catchError(this.handleError('postFile', []))
    );
  }

  confirmPayment(body): Observable<any> {
    return this.http.post<any>(this.confirmPaymentUrl, body, {headers: new HttpHeaders()}).pipe(
      tap((data) => { 
        console.log(data);
      }),
      catchError(this.handleError<any>('confirmPayment'))
    );    
  }


}
